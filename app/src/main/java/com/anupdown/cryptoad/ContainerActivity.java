package com.anupdown.cryptoad;

import android.app.Activity;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.anupdown.cryptoad.model.stats;
import com.anupdown.cryptoad.utils.CurrencyRecyclerAdapter;
import com.anupdown.cryptoad.utils.WebService;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ContainerActivity extends AppCompatActivity implements SwipeRefreshLayout.OnRefreshListener{

    private Activity activity;
    public static final String URL_KOINEX_TICKER = "https://koinex.in/api/ticker";
    public static final String URL_COINOME_TICKER = "https://www.coinome.com/api/v1/ticker.json";

    /*@BindView(R.id.tv_prices)
    TextView tvPrices;*/
    private WebService webService;
    @BindView(R.id.recycler_crypto_rates)
    RecyclerView recyclerView;

    @BindView(R.id.swipe_container)
    SwipeRefreshLayout mSwipeRefreshLayout;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_container);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        activity = this;
        ButterKnife.bind(this);
        webService = new WebService();

        /*FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mSwipeRefreshLayout.setRefreshing(true);

                fetchRates(webService);
                *//*Snackbar.make(view, "Refresh", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();*//*
            }
        });*/


        init();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_container_activity, menu);

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();

        return super.onOptionsItemSelected(item);
    }

    private void init() {


        // SwipeRefreshLayout
        mSwipeRefreshLayout.setOnRefreshListener(this);
        mSwipeRefreshLayout.setColorSchemeResources(R.color.colorPrimary,
                android.R.color.holo_green_dark,
                android.R.color.holo_orange_dark,
                android.R.color.holo_blue_dark);

        /**
         * Showing Swipe Refresh animation on activity create
         * As animation won't start on onCreate, post runnable is used
         */
        mSwipeRefreshLayout.post(new Runnable() {

            @Override
            public void run() {

                mSwipeRefreshLayout.setRefreshing(true);

                // Fetching data from server
                fetchRates(webService);
            }
        });

//        fetchRates(webService);

    }

    private void fetchRates(final WebService webService) {
        webService.getWebServiceCall(URL_KOINEX_TICKER, activity, false, new WebService.VolleyCallback() {
            @Override
            public void onSuccess(String result) {
                /*{
                    "prices": {
                    "inr":{
                    "BCH": "85000.0",
                            "BTC": "516000.0",
                            "ETH": "22200.0",
                            "GNT": 13.53,
                            "LTC": "4485.0",
                            "MIOTA": 51.98,
                            "OMG": 495.4,
                            "XRP": "15.0"
                    }
                }
                ,"stats": {
                    "BCH": {
                        "highest_bid": "84198.0",
                                "last_traded_price": "85000.0",
                                "lowest_ask": "85000.0",
                                "max_24hrs": "89999.0",
                                "min_24hrs": "67500.0",
                                "vol_24hrs": "3280.18"
                    },

                }
                }*/



                HashMap<String, stats> mapCurrencies = new HashMap<>();

                try {
                    JSONObject rootObject = new JSONObject(result);

                    //get prices jsonObject
                    JSONObject jsonObjectPrices = rootObject.getJSONObject("prices");

                    JSONObject jsonObjectInr = jsonObjectPrices.getJSONObject("inr");
                    //lists all crypto shortforms on ticker
                    Iterator keys = jsonObjectInr.keys();

                    //process "prices" json object
                    while (keys.hasNext()) {

                        String key = (String) keys.next();
                        String lastTradedPrice = jsonObjectInr.getString(key);

                        stats mStats = new stats();
                        mStats.setLast_traded_price(lastTradedPrice);
                        mStats.setShort_name(key);
                        mapCurrencies.put(key, mStats);
                    }

                    JSONObject jsonObjectStats = rootObject.getJSONObject("stats");

                    JSONObject jsonObjectInrStats = jsonObjectStats.getJSONObject("inr");
                    Gson gson = new Gson();
                    for (String key : mapCurrencies.keySet()) {
                        if (jsonObjectInrStats.has(key)) {

                            //for each key in map get stats
                            JSONObject jsonObjectInrStat = jsonObjectStats.getJSONObject("inr");
                            JSONObject jsonObjectCurrencyStat = jsonObjectInrStat.getJSONObject(key);
                            stats currencyStats = gson.fromJson(jsonObjectCurrencyStat.toString(), stats.class);

                            currencyStats.setShort_name(key);

                            if (currencyStats.getLast_traded_price() == null) {

                                currencyStats.setLast_traded_price(mapCurrencies.get(key).getLast_traded_price());
                            }

                            //add missing stats to hash map
                            mapCurrencies.put(key, currencyStats);
//                            Log.e(key + "-vol24hrs", mapCurrencies.get(key).getVol_24hrs());
                        }
                    }

                    List<stats> statsArrayList = new ArrayList<>(mapCurrencies.values());

                    setupRecyclerView(statsArrayList);
                } catch (JSONException e) {
                    Toast.makeText(activity,e.toString(),Toast.LENGTH_LONG).show();
                    e.printStackTrace();
                }

            }

            @Override
            public void onError(String error) {
                mSwipeRefreshLayout.setRefreshing(false);

                Snackbar.make(getWindow().getDecorView(), "Failed to refresh", Snackbar.LENGTH_INDEFINITE)
                        .setAction("RETRY", new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                fetchRates(webService);
                            }
                        }).show();
            }
        });
    }

    private void setupRecyclerView(List<stats> statsArrayList) {
        recyclerView.setLayoutManager(new LinearLayoutManager(activity));
        recyclerView.setAdapter(new CurrencyRecyclerAdapter(activity,statsArrayList));
        mSwipeRefreshLayout.setRefreshing(false);

    }

    @Override
    public void onRefresh() {
        fetchRates(webService);
    }
}
