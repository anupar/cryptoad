package com.anupdown.cryptoad;


/**
 * Created by anoop.r on 09/12/17.
 */

import android.appwidget.AppWidgetManager;
import android.content.Context;
import android.content.Intent;
import android.widget.RemoteViews;
import android.widget.RemoteViewsService.RemoteViewsFactory;

import com.anupdown.cryptoad.model.stats;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Locale;

/**
 * here it now takes RemoteFetchService ArrayList<ListItem> for data
 * which is a static ArrayList
 * and this example won't work if there are multiple widgets and
 * they update at same time i.e they modify RemoteFetchService ArrayList at same
 * time.
 */
public class RatesListProvider implements RemoteViewsFactory {
    private ArrayList<stats> statsList = new ArrayList<stats>();
    private Context mContext = null;
    private int appWidgetId;

    public RatesListProvider(Context context, Intent intent) {
        this.mContext = context;
        appWidgetId = intent.getIntExtra(AppWidgetManager.EXTRA_APPWIDGET_ID,
                AppWidgetManager.INVALID_APPWIDGET_ID);

        populateListItem();
    }

    private void populateListItem() {
        if(RemoteRateFetchService.statsArrayList !=null )
            statsList = (ArrayList<stats>) RemoteRateFetchService.statsArrayList.clone();
        else
            statsList = new ArrayList<>();

    }

    @Override
    public int getCount() {
        return statsList.size();
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    /*
     *Similar to getView of Adapter where instead of View
     *we return RemoteViews
     *
     */
    @Override
    public RemoteViews getViewAt(int position) {
        final RemoteViews remoteView = new RemoteViews(
                mContext.getPackageName(), R.layout.list_view_widget_item);
        stats statsItem = statsList.get(position);


        String last_traded_price = (statsList.get(position).getLast_traded_price());

        last_traded_price = last_traded_price!=null?formatLakh(last_traded_price):null;

        remoteView.setTextViewText(R.id.tv_price, last_traded_price);


        String short_name = statsList.get(position).getShort_name();
        if (short_name != null) {
            String resourceName = getResourceName(short_name);
            int imageId = mContext.getResources().getIdentifier(resourceName, "drawable", mContext.getPackageName());
            remoteView.setImageViewResource(R.id.iv_currency_logo,imageId);
        }
        remoteView.setTextViewText(R.id.tv_name,short_name);

        return remoteView;
    }



    @Override
    public RemoteViews getLoadingView() {
        return null;
    }

    @Override
    public int getViewTypeCount() {
        return 1;
    }

    @Override
    public boolean hasStableIds() {
        return true;
    }

    @Override
    public void onCreate() {
    }

    @Override
    public void onDataSetChanged() {
    }

    @Override
    public void onDestroy() {
    }

    /**
     * Format string to double with Indian standard system.
     * */
    private static String formatLakh(String string) {
        Double d = Double.parseDouble(string);
        String s = String.format(Locale.UK, "%1.2f", Math.abs(d));
        s = s.replaceAll("(.+)(...\\...)", "$1,$2");
        while (s.matches("\\d{3,},.+")) {
            s = s.replaceAll("(\\d+)(\\d{2},.+)", "$1,$2");
        }
        return d < 0 ? ("-" + s) : s;
    }

    public String getResourceName(String shortName) {
        HashMap<String, String> imageNameMap = new HashMap<>();
        imageNameMap.put("BTC", "ic_bitcoin");
        imageNameMap.put("BCH", "ic_bitcoin_cash");
        imageNameMap.put("ETH", "ic_ethereum");
        imageNameMap.put("XRP", "ic_ripple");
        imageNameMap.put("LTC", "ic_litecoin");
        imageNameMap.put("GNT", "ic_golem");
        imageNameMap.put("OMG", "ic_omg");
        imageNameMap.put("MIOTA", "ic_miota");

        return imageNameMap.get(shortName);
    }

}