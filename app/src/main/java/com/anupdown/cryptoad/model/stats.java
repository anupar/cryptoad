package com.anupdown.cryptoad.model;

/**
 * Created by anoop.r on 18/11/17.
 */

public class stats {
     /*"BCH": {
        "highest_bid": "84198.0",
                "last_traded_price": "85000.0",
                "lowest_ask": "85000.0",
                "max_24hrs": "89999.0",
                "min_24hrs": "67500.0",
                "vol_24hrs": "3280.18"
    }*/
     String highest_bid
             ,last_traded_price
             ,lowest_ask
             ,max_24hrs
             ,min_24hrs
             ,vol_24hrs, short_name, currency_full_form;

    public String getCurrency_full_form() {
        return currency_full_form;
    }

    public void setCurrency_full_form(String currency_full_form) {
        this.currency_full_form = currency_full_form;
    }

    public String getShort_name() {
          return short_name;
     }

     public void setShort_name(String short_name) {
          this.short_name = short_name;
     }

     public String getHighest_bid() {
          return highest_bid;
     }

     public void setHighest_bid(String highest_bid) {
          this.highest_bid = highest_bid;
     }

     public String getLast_traded_price() {
          return last_traded_price;
     }

     public void setLast_traded_price(String last_traded_price) {
          this.last_traded_price = last_traded_price;
     }

     public String getLowest_ask() {
          return lowest_ask;
     }

     public void setLowest_ask(String lowest_ask) {
          this.lowest_ask = lowest_ask;
     }

     public String getMax_24hrs() {
          return max_24hrs;
     }

     public void setMax_24hrs(String max_24hrs) {
          this.max_24hrs = max_24hrs;
     }

     public String getMin_24hrs() {
          return min_24hrs;
     }

     public void setMin_24hrs(String min_24hrs) {
          this.min_24hrs = min_24hrs;
     }

     public String getVol_24hrs() {
          return vol_24hrs;
     }

     public void setVol_24hrs(String vol_24hrs) {
          this.vol_24hrs = vol_24hrs;
     }
}
