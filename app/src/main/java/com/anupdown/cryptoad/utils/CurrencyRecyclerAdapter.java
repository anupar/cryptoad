package com.anupdown.cryptoad.utils;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.anupdown.cryptoad.R;
import com.anupdown.cryptoad.model.stats;
import com.bumptech.glide.Glide;

import java.util.HashMap;
import java.util.List;
import java.util.Locale;

/**
 * Created by anoop.r on 25/11/17.
 */

public class CurrencyRecyclerAdapter extends RecyclerView.Adapter<CurrencyRecyclerAdapter.ViewHolder> {
    private final List<stats> statsList;
    private final Context mContext;

    // Provide a reference to the views for each data item
    // Complex data items may need more than one view per item, and
    // you provide access to all the views for a data item in a view holder
    public static class ViewHolder extends RecyclerView.ViewHolder {
        // each data item is just a string in this case
        public View mView;
        public TextView tvPrice;
        public TextView tv24hrHigh;
        public TextView tv24hrLow;
        public ImageView ivCurrencyLogo;
        public TextView tvName;
        public ViewHolder(View view) {
            super(view);
            mView = view;
            tvPrice = view.findViewById(R.id.tv_price);
            tv24hrHigh = view.findViewById(R.id.tv_24hr_high);
            tv24hrLow = view.findViewById(R.id.tv_24hr_low);
            ivCurrencyLogo = view.findViewById(R.id.iv_currency_logo);
            tvName = view.findViewById(R.id.tv_name);
        }
    }

    // Provide a suitable constructor (depends on the kind of dataset)
    public CurrencyRecyclerAdapter(Context mContext, List<stats> statsList) {
        this.mContext = mContext;
        this.statsList = statsList;
    }

    // Create new views (invoked by the layout manager)
    @Override
    public CurrencyRecyclerAdapter.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                                 int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.card_view_crypto_rates, parent, false);
        return new ViewHolder(view);
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        // - get element from your dataset at this position
        // - replace the contents of the view with that element
        String last_traded_price = (statsList.get(position).getLast_traded_price());
        String max_24hrs = (statsList.get(position).getMax_24hrs());
        String min_24hrs = (statsList.get(position).getMin_24hrs());

        last_traded_price = last_traded_price!=null?formatLakh(last_traded_price):null;
        max_24hrs = max_24hrs!=null?formatLakh(max_24hrs):null;
        min_24hrs = min_24hrs!=null?formatLakh(min_24hrs):null;

        holder.tvPrice.setText(last_traded_price);
        holder.tv24hrHigh.setText(max_24hrs);
        holder.tv24hrLow.setText(min_24hrs);

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                Toast.makeText(mContext, "click", Toast.LENGTH_SHORT).show();
            }
        });
        String short_name = statsList.get(position).getShort_name();
        String full_name = statsList.get(position).getCurrency_full_form();

        String uri = AppConstants.URL_CRYPTO_LOGO.replace(AppConstants.SHORTNAME_PLACEHOLDER, short_name.toLowerCase());
        String alternateUri = null;
        if (full_name != null)
            alternateUri = AppConstants.URL_CRYPTO_LOGO.replace(AppConstants.SHORTNAME_PLACEHOLDER, full_name.toLowerCase());
        Glide.with(mContext)
                .load(uri)
                .error(Glide.with(mContext)
                        .load(alternateUri))
                .into(holder.ivCurrencyLogo);
        holder.tvName.setText(short_name);
    }

    /**
     * Format string to double with Indian standard system.
     * */
    private static String formatLakh(String string) {
        Double d = Double.parseDouble(string);
        String s = String.format(Locale.UK, "%1.2f", Math.abs(d));
        s = s.replaceAll("(.+)(...\\...)", "$1,$2");
        while (s.matches("\\d{3,},.+")) {
            s = s.replaceAll("(\\d+)(\\d{2},.+)", "$1,$2");
        }
        return d < 0 ? ("-" + s) : s;
    }
    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return statsList.size();
    }

    public String getResourceName(String shortName) {
        HashMap<String, String> imageNameMap = new HashMap<>();
        imageNameMap.put("BTC", "ic_bitcoin");
        imageNameMap.put("BCH", "ic_bitcoin_cash");
        imageNameMap.put("ETH", "ic_ethereum");
        imageNameMap.put("XRP", "ic_ripple");
        imageNameMap.put("LTC", "ic_litecoin");
        imageNameMap.put("GNT", "ic_golem");
        imageNameMap.put("OMG", "ic_omg");
        imageNameMap.put("MIOTA", "ic_miota");
        imageNameMap.put("REQ", "ic_req");

        return imageNameMap.get(shortName);
    }
}