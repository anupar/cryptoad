package com.anupdown.cryptoad.utils;

import android.app.ProgressDialog;
import android.content.Context;
import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NetworkResponse;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;


public class WebService {

    private static final String LOADING_MESSAGE = "Loading...";
    static String responseData;
    ProgressDialog loading = null;
    boolean isShowLoading = false;

    public void getWebServiceCall(String url, Context context, final boolean isShowLoading, final VolleyCallback volleyCallback) {

        this.isShowLoading = isShowLoading;
        RequestQueue queue = Volley.newRequestQueue(context);

        if (isShowLoading)
            setupLoadingDialog(context);

        StringRequest stringRequest = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {


            @Override
            public void onResponse(String stringResponse) {
                responseData = stringResponse;
                volleyCallback.onSuccess(responseData);
//                Log.e("json response",responseData);
                if (loading != null && isShowLoading) {
                    loading.dismiss();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
//                responseData[0] = "null";
                NetworkResponse networkResponse = error.networkResponse;
                if (networkResponse != null) {
                    Log.e("Volley", "Error. HTTP Status Code:" + networkResponse.statusCode);
                }

                if (error instanceof TimeoutError) {
                    Log.e("Volley timeout Error", "TimeoutError");
                } else if (error instanceof NoConnectionError) {
                    Log.e("Volley connection Error", "NoConnectionError");
                } else if (error instanceof AuthFailureError) {
                    Log.e("Volley auth Error", "AuthFailureError");
                } else if (error instanceof ServerError) {
                    Log.e("Volley server Error", "ServerError");
                    error.printStackTrace();
                } else if (error instanceof NetworkError) {
                    Log.e("Volley network Error", "NetworkError");
                } else if (error instanceof ParseError) {
                    Log.e("Volley parse Error", "ParseError");
                }

                String msg = error.toString();
                Log.e("volley error", msg);
                volleyCallback.onError(msg);
                if (isShowLoading)
                    loading.dismiss();
            }

        })/*{
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();

                //proxy cred
                String credentials = "user:pass";
                String auth = "Basic "
                        + Base64.encodeToString(credentials.getBytes(), Base64.NO_WRAP);
//                if (AppConstants.URL_GUIDE_ME == AppConstants.URL_GUIDE_ME_PRODUCTION)
                headers.put("Authorization", auth);

                return headers;
            }
        }*/;

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0,0,0));
        queue.add(stringRequest);
//        return responseData[0];
    }

    private void setupLoadingDialog(Context context) {
        loading = new ProgressDialog(context);
        loading.setMessage(LOADING_MESSAGE);
        loading.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        loading.setCancelable(false);
        loading.show();
    }

    public interface VolleyCallback{
        void onSuccess(String result);
        void onError(String error);
    }
}
