package com.anupdown.cryptoad.utils;

/**
 * Created by anoop.r on 01/03/18.
 */

public final class AppConstants {

    public static final String KOINEX = "KOINEX";
    public static final String COINDELTA = "COINDELTA";
    public static final String COINOME = "COINOME";
    public static final String ZEBPAY = "ZEBPAY";

    public static final String ARG_EXCHANGE = "EXCHANGE";

    public static final long REFRESH_INTERVAL = 70000;
    public static final String SHORTNAME_PLACEHOLDER = "currencyName";
    public static final String URL_CRYPTO_LOGO = "https://raw.githubusercontent.com/cjdowner/cryptocurrency-icons/master/128/color/" + SHORTNAME_PLACEHOLDER + ".png";

}
