package com.anupdown.cryptoad;

import android.app.Service;
import android.appwidget.AppWidgetManager;
import android.content.Intent;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.anupdown.cryptoad.model.stats;
import com.anupdown.cryptoad.utils.WebService;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

/**
 * Created by anoop.r on 08/12/17.
 */

public class RemoteRateFetchService extends Service {


    private static final String CHANNEL_ID = "channel1";
    private static final int NOTIFICATION_ID = -1;
    private int appWidgetId = AppWidgetManager.INVALID_APPWIDGET_ID;
    public static final String URL_KOINEX_TICKER = "https://koinex.in/api/ticker";
    public static ArrayList<stats> statsArrayList;

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    /*
	 * Retrieve appwidget id from intent it is needed to update widget later
	 * initialize our WebService class
	 */
    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        if (intent.hasExtra(AppWidgetManager.EXTRA_APPWIDGET_ID))
            appWidgetId = intent.getIntExtra(
                    AppWidgetManager.EXTRA_APPWIDGET_ID,
                    AppWidgetManager.INVALID_APPWIDGET_ID);

        fakeStartForeground();
        fetchRates(new WebService());
        return super.onStartCommand(intent, flags, startId);
    }
    private void fetchRates(WebService webService) {
        webService.getWebServiceCall(URL_KOINEX_TICKER, getBaseContext(), false, new WebService.VolleyCallback() {
            @Override
            public void onSuccess(String result) {
                /*{
                    "prices": {
                    "BCH": "85000.0",
                            "BTC": "516000.0",
                            "ETH": "22200.0",
                            "GNT": 13.53,
                            "LTC": "4485.0",
                            "MIOTA": 51.98,
                            "OMG": 495.4,
                            "XRP": "15.0"
                }
                ,"stats": {
                    "BCH": {
                        "highest_bid": "84198.0",
                                "last_traded_price": "85000.0",
                                "lowest_ask": "85000.0",
                                "max_24hrs": "89999.0",
                                "min_24hrs": "67500.0",
                                "vol_24hrs": "3280.18"
                    },

                }
                }*/



                HashMap<String, stats> mapCurrencies = new HashMap<>();

                try {
                    JSONObject rootObject = new JSONObject(result);

                    //get prices jsonObject
                    JSONObject jsonObjectPrices = rootObject.getJSONObject("prices");

                    //lists all crypto shortforms on ticker
                    Iterator keys = jsonObjectPrices.keys();

                    //process "prices" json object
                    while (keys.hasNext()) {

                        String key = (String) keys.next();
                        String lastTradedPrice = jsonObjectPrices.getString(key);

                        stats mStats = new stats();
                        mStats.setLast_traded_price(lastTradedPrice);
                        mStats.setShort_name(key);
                        mapCurrencies.put(key, mStats);
                    }

                    JSONObject jsonObjectStats = rootObject.getJSONObject("stats");

                    Gson gson = new Gson();
                    for (String key : mapCurrencies.keySet()) {
                        if (jsonObjectStats.has(key)) {

                            //for each key in map get stats
                            JSONObject jsonObjectCurrencyStat = jsonObjectStats.getJSONObject(key);
                            stats currencyStats = gson.fromJson(jsonObjectCurrencyStat.toString(), stats.class);

                            currencyStats.setShort_name(key);
                            //add missing stats to hash map
                            mapCurrencies.put(key, currencyStats);
//                            Log.e(key + "-vol24hrs", mapCurrencies.get(key).getVol_24hrs());
                        }
                    }

                    statsArrayList = new ArrayList<>(mapCurrencies.values());

                    populateWidget();
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void onError(String error) {
                Log.e("volleyError!", error);
            }
        });
    }

    /**
     * Method which sends broadcast to WidgetProvider
     * so that widget is notified to do necessary action
     * and here action == WidgetProvider.DATA_FETCHED
     */
    private void populateWidget() {

        Intent widgetUpdateIntent = new Intent(this,CryptoRateWidgetProvider.class);
        widgetUpdateIntent.setAction(CryptoRateWidgetProvider.DATA_FETCHED);
        widgetUpdateIntent.putExtra(AppWidgetManager.EXTRA_APPWIDGET_ID,
                appWidgetId);
        sendBroadcast(widgetUpdateIntent);

        this.stopSelf();
    }
    private void fakeStartForeground() {
        NotificationCompat.Builder builder =
                new NotificationCompat.Builder(this, CHANNEL_ID)
                        .setContentTitle("")
                        .setContentText("");

        startForeground(NOTIFICATION_ID, builder.build());
    }
}
