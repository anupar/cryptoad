package com.anupdown.cryptoad;

import android.app.PendingIntent;
import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.util.Log;
import android.view.View;
import android.widget.RemoteViews;
import android.widget.Toast;

/**
 * Implementation of App Widget functionality.
 */
public class CryptoRateWidgetProvider extends AppWidgetProvider {

    // String to be sent on Broadcast as soon as Data is Fetched
    // should be included on WidgetProvider manifest intent action
    // to be recognized by this WidgetProvider to receive broadcast
    public static final String DATA_FETCHED = "com.anupdown.cryptoad.DATA_FETCHED";
    public static final String WIDGET_CLICK = "com.anupdown.cryptoad.WIDGET_CLICK";
    private static final String LOG_TAG = "com.anupdown.cryptoad";
    private static final String REFRESH_CLICK = "com.anupdown.cryptoad.REFRESH_CLICK";

    static void updateAppWidget(Context context, AppWidgetManager appWidgetManager,
                                int appWidgetId) {

        // Construct the RemoteViews object
        RemoteViews views = new RemoteViews(context.getPackageName(), R.layout.crypto_rate_widget);
        views.setTextViewText(R.id.tv_price, "");

        // Instruct the widget manager to update the widget
        appWidgetManager.updateAppWidget(appWidgetId, views);
    }

    @Override
    public void onUpdate(Context mContext, AppWidgetManager appWidgetManager, int[] appWidgetIds) {

        RemoteViews remoteViews = new RemoteViews(mContext.getPackageName(),
                R.layout.crypto_rate_widget);

        remoteViews.setOnClickPendingIntent(R.id.rl_crypto_rate_widget,
                getPendingSelfIntent(mContext, WIDGET_CLICK));
        /*remoteViews.setOnClickPendingIntent(R.id.rl_crypto_rate_widget,
                getPendingSelfIntent(mContext, WIDGET_CLICK));
        remoteViews.setOnClickPendingIntent(R.id.ll_list_view,
                getPendingSelfIntent(mContext, WIDGET_CLICK));
        remoteViews.setOnClickPendingIntent(R.id.lv_rate_list,
                getPendingSelfIntent(mContext, WIDGET_CLICK));*/

        remoteViews.setViewVisibility(R.id.pb_widget, View.VISIBLE);
        // There may be multiple widgets active, so update all of them
        for (int appWidgetId : appWidgetIds) {

            Log.e(LOG_TAG, "Widget load");
            Intent serviceIntent = new Intent(mContext, RemoteRateFetchService.class);
            serviceIntent.putExtra(AppWidgetManager.EXTRA_APPWIDGET_ID,
                    appWidgetId);

            if (Build.VERSION.SDK_INT > Build.VERSION_CODES.N_MR1) {
                mContext.startForegroundService(serviceIntent);
            } else {
                mContext.startService(serviceIntent);
            }
        }
        super.onUpdate(mContext, appWidgetManager, appWidgetIds);
    }

    @Override
    public void onEnabled(Context context) {
        // Enter relevant functionality for when the first widget is created
    }

    @Override
    public void onDisabled(Context context) {
        // Enter relevant functionality for when the last widget is disabled
    }

    private RemoteViews updateWidgetListView(Context context, int appWidgetId) {

        // which layout to show on widget
        RemoteViews remoteViews = new RemoteViews(context.getPackageName(),
                R.layout.crypto_rate_widget);

        // RemoteViews Service needed to provide adapter for ListView
        Intent svcIntent = new Intent(context, WidgetService.class);
        // passing app widget id to that RemoteViews Service
        svcIntent.putExtra(AppWidgetManager.EXTRA_APPWIDGET_ID, appWidgetId);
        // setting a unique Uri to the intent
        // don't know its purpose to me right now
        svcIntent.setData(Uri.parse(svcIntent.toUri(Intent.URI_INTENT_SCHEME)));
        // setting adapter to listview of the widget
        remoteViews.setRemoteAdapter(appWidgetId, R.id.lv_rate_list,
                svcIntent);
        remoteViews.setViewVisibility(R.id.pb_widget, View.GONE);
        // setting an empty view in case of no data
        remoteViews.setEmptyView(R.id.lv_rate_list, R.id.empty_view);
        return remoteViews;
    }

    /*
	 * It receives the broadcast as per the action set on intent filters on
	 * Manifest.xml once data is fetched from RemotePostService,it sends
	 * broadcast and WidgetProvider notifies to change the data the data change
	 * right now happens on ListProvider as it takes RemoteFetchService
	 * listItemList as data
	 */
    @Override
    public void onReceive(Context context, Intent intent) {
        super.onReceive(context, intent);
        String action = intent.getAction();
        if (action!=null)
        if (action.equals(DATA_FETCHED)) {
            int appWidgetId = intent.getIntExtra(
                    AppWidgetManager.EXTRA_APPWIDGET_ID,
                    AppWidgetManager.INVALID_APPWIDGET_ID);
            AppWidgetManager appWidgetManager = AppWidgetManager
                    .getInstance(context);
            RemoteViews remoteViews = updateWidgetListView(context, appWidgetId);
            appWidgetManager.updateAppWidget(appWidgetId, remoteViews);
        } else if (action.equals(REFRESH_CLICK)) {
            Log.e(LOG_TAG, "on receive REFRESH_CLICK ");

            Toast.makeText(context,"click",Toast.LENGTH_SHORT).show();
            reloadRates(context);
        }

    }

    private void reloadRates(Context mContext) {

        AppWidgetManager am = AppWidgetManager.getInstance(mContext);
        ComponentName thisWidget = new ComponentName(mContext, CryptoRateWidgetProvider.class);
        int[] appWidgetIds = am.getAppWidgetIds(thisWidget);

//            updateAppWidget(context, appWidgetManager, appWidgetId);
        Log.e(LOG_TAG, "Widget reload");

        for (int appWidgetId : appWidgetIds) {

            Intent serviceIntent = new Intent(mContext, RemoteRateFetchService.class);
            serviceIntent.putExtra(AppWidgetManager.EXTRA_APPWIDGET_ID,
                    appWidgetId);

            if (Build.VERSION.SDK_INT > Build.VERSION_CODES.N_MR1) {
                mContext.startForegroundService(serviceIntent);
            } else {
                mContext.startService(serviceIntent);
            }
        }
    }

    protected PendingIntent getPendingSelfIntent(Context context, String action) {
        Log.e(LOG_TAG, "getPendingSelfIntent: fired");
        Intent intent = new Intent(context, CryptoRateWidgetProvider.class);
        intent.setAction(action);
        return PendingIntent.getBroadcast(context, 0, intent, 0);
    }
}

